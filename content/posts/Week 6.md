+++
title = "Week 6 project"
description = "Instrument a Rust Lambda Function with Logging and Tracing"
date = 2024-03-08
[taxonomies]
tags= ["Zola", "Theme", "Markdown", "Typography"]
+++
Create a static site with ZolaLinks to an external site., a Rust static site generator, that will hold all of the portfolio work in this class.  Store source code in a GitLab repo in our Duke GitLab organization.

## Requirements
    Add logging to a Rust Lambda function
    Integrate AWS X-Ray tracing
    Connect logs/traces to CloudWatch

## Deliverables
    Updated Rust code
    Screenshots or demo video showing logs and traces

[Gitlab Repo link](https://gitlab.com/SKN007/mini-project6.git)