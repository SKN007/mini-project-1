+++
title = "Week 1 project"
description = "Create a static site with Zola"
date = 2024-02-02
[taxonomies]
tags= ["Zola", "Theme", "Markdown", "Typography"]
+++
Create a static site with ZolaLinks to an external site., a Rust static site generator, that will hold all of the portfolio work in this class.  Store source code in a GitLab repo in our Duke GitLab organization.

## Requirements
    Static site generated with Zola
    Home page and portfolio project page templates
    Styled with CSS
    GitLab repo with source code

## Deliverables
    Link to live static site or screenshot showing it running
    Link to GitLab repo

[Gitlab Repo link](https://gitlab.com/SKN007/mini-project1.git)