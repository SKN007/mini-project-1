+++
title = "Week 4 project"
description = "Containerize a Rust Actix Web Service"
date = 2024-02-23
[taxonomies]
tags= ["Zola", "Theme", "Markdown", "Typography"]
+++
Containerize a Rust Actix Web Service
## Requirements
    Containerize simple Rust Actix web app
    Build Docker image
    Run container locally

## Deliverables
    Dockerfile
    Screenshots or demo video showing container running
    Writeup of process

[Gitlab Repo link](https://gitlab.com/SKN007/mini-project4.git)