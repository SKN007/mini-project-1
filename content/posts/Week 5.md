+++
title = "Week 5 project"
description = "Serverless Rust Microservice "
date = 2024-02-21
[taxonomies]
tags= ["Zola", "Theme", "Markdown", "Typography"]
+++
Serverless Rust Microservice 

## Requirements
    Create a Rust AWS Lambda function (or app runner)
    Implement a simple service
    Connect to a database

## Deliverables
    Rust code
    Screenshots or demo video showing successful invocation
    Writeup explaining service

[Gitlab Repo link](https://gitlab.com/SKN007/mini-project5.git)