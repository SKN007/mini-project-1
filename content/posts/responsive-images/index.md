+++
title = "Welcome to my blog!"
date = 2024-01-26
description = "My first post."
[taxonomies]

[extra]
images= ["1.png"]


+++

Here is my IDS721 course blog, I will post my works here.

{{ image(src="1.png", alt="Example image") }}

<!-- You can include responsive images using the image shortcode like so:

```md
\{\{ image(src="yourimage.jpg", alt="This is my image") }}
``` -->
