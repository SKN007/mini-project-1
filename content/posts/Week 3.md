+++
title = "Week 3 project"
description = "Create an S3 Bucket using CDK with AWS CodeWhisperer. "
date = 2024-02-16
[taxonomies]
tags= ["Zola", "Theme", "Markdown", "Typography"]
+++
Create an S3 Bucket using CDK with AWS CodeWhisperer. 

## Requirements
    Create S3 bucket using AWS CDK
    Use CodeWhisperer to generate CDK code
    Add bucket properties like versioning and encryption

## Deliverables
    CDK app code
    Generated S3 bucket screenshot
    Writeup explaining CodeWhisperer usage

[Gitlab Repo link](https://gitlab.com/SKN007/mini-project3.git)