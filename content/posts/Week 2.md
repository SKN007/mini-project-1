+++
title = "Week 2 project"
description = "Create a simple AWS Lambda function that processes data."
date = 2024-02-11
[taxonomies]
tags= ["Zola", "Theme", "Markdown", "Typography"]
+++
Create a simple AWS Lambda function that processes data.

## Requirements
    Rust Lambda Function using Cargo LambdaLinks to an external site.
    Process and transform sample data

## Deliverables
    Lambda functionality: 30%
    API Gateway integration: 30%
    Data processing: 30%
    Documentation: 10%

[Gitlab Repo link](https://gitlab.com/SKN007/mini-project2.git)