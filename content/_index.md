+++
title = "Zola-Inky"
images = []
template = "index.html"
in_search_index = true
+++

# About this course

[IDS 721: Data Analysis at Scale in Cloud](https://scholars.duke.edu/course/IDS721) will provide students with extensive hands-on experience manipulating real (often messy, error ridden, and poorly documented) data using the a range of bread-and-butter data science tools (like the command line, git, python (especially numpy and pandas), jupyter notebooks, and more). The goal of these exercises is to ensure students are comfortable working with data in most any form.

---

## Kunning Shen

Duke ECE ms student.
