
[![pipeline status](https://gitlab.com/SKN007/mini-project-1/badges/main/pipeline.svg)](https://gitlab.com/SKN007/mini-project-1/-/commits/main)

# Individual Project 1


I created a static site with Zola, a Rust static site generator, that will hold all of my portfolio work in this class.

[My website](https://mini-project-1-skn007-db222db513b09df8d5d7815ef5968c98a0606bd8d.gitlab.io)

## Requirements

    Website built with ZolaLinks to an external site., Hugo, Gatsby, Next.js or equivalent

    GitLab workflow to build and deploy site on push

    Hosted on Vercel, Netlify, AWS Amplify, AWS S3, or others.

## Steps
1. Install Zola.

2. Create a new website with Zola.

 ```
zola init personal_website
```
3. Choose and use a theme in Zola's official website and alter the code to meet my needs.

4. Test, build and deploy the website

```
zola serve
```
## Demo Video
You can directly access to [my website](https://mini-project-1-skn007-db222db513b09df8d5d7815ef5968c98a0606bd8d.gitlab.io) or watch the video "Demo video.mp4"